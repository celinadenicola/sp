<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div>
                    <h3 class="title m_title">PPRODUCTOS Y SERVICIOS</h3>
                    <div class="sbs">
                        <ul class="menu">
                            <li><a href="antientradera.php">Antientradera</a></li>
                            <li><a href="incendio.php">Incencio</a></li>
                            <li><a href="monitoreo.php">Monitoreo</a></li>
                            <li><a href="camaras.php">Cámaras</a></li>
                            <li><a href="fisica.php">Seguridad física</a></li>
                            <li><a href="http://www.atemponet.com">Teleasistencia domiciliaria</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!--/ col-sm-5 -->

            <div class="col-sm-4">
                <div class="newsletter-signup">
                    <h3 class="title m_title">SUSCRIPCIÓN A NEWSLETTER </h3>
                    <form action="http://YOUR_USERNAME.DATASERVER.list-manage.com/subscribe/post-json?u=YOUR_API_KEY&amp;id=LIST_ID&c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <input type="email" value="" name="EMAIL" class="nl-email form-control" id="mce-EMAIL" placeholder="Email" required>
                        <input type="submit" name="subscribe" class="nl-submit" id="mc-embedded-subscribe" value="ENVIAR">
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;">
                            <input type="text" name="b_xxxxxxxxxxxxxxxxxxxCUSTOMxxxxxxxxx" value="">
                        </div>
                    </form>
                    <div id="notification_container"></div>
                </div><!-- end newsletter-signup -->
            </div>
            <!-- col-sm-4 -->
            <div class="col-sm-1">

            </div>
            <div class="col-sm-3">
                <div>
                    <h3 class="title m_title">CONTACTO</h3>
                    <div><p><strong> Avenida 13 N° 716 La Plata - Bs As </strong><br>
                        <p>TEL: +54 (221) 423 4141 <br>
                            FAX: +54 (221) 423 1098 </p>
                    </div>
                </div>
            </div>
            <!--/ col-sm-3 -->
        </div>
        <!--/ row -->



        <div class="row">
            <div class="col-sm-12">
                <div class="bottom clearfix">
                    <!-- social-icons -->
                    <ul class="social-icons sc--clean clearfix">
                       
                        <li><a href="https://www.facebook.com/saseguridadprivadasa/?fref=ts" target="_self" class="icon-facebook" title="Facebook"></a></li>

                    </ul>
                    <!--/ social-icons -->

                    <!-- copyright -->
                    <div class="copyright">
                        <a href="index.php">
                            <img src="images/footer-logo.png" alt="SP Seguridad Privada">
                        </a>
                        <p>© 2016 Todos los derechos reservados. Diseño <a href="http://twww.estudioumo.com.ar">UMO</a>.</p>
                    </div>
                    <!--/ copyright -->
                </div>
                <!--/ bottom -->
            </div>
            <!--/ col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</footer>
<!--/ Footer - Default Style -->