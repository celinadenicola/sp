<!-- Header style 1 -->
<header id="header" class="site-header style1 cta_button">
    <!-- header bg -->
    <div class="kl-header-bg"></div>
    <!--/ header bg -->

    <!-- siteheader-container -->
    <div class="container siteheader-container">
        <!-- top-header -->
        <div class="kl-top-header clearfix">
            <!-- HEADER ACTION -->
            <div class="header-links-container ">


            </div>
            <!--/ HEADER ACTION -->

            <!-- HEADER ACTION left -->
            <div class="header-links-container">
                <!-- Header Social links -->
                <ul class="social-icons sc--clean topnav navRight">
                    <li><a href="https://www.facebook.com/saseguridadprivadasa/?fref=ts" target="_self" class="icon-facebook" title="Facebook"></a></li>


                </ul>

                <!-- header contact text -->
                <span class="kl-header-toptext">


				<i class="fa fa-phone social-icons sc--clean" aria-hidden="true"></i>
               54 (221) 423 4141 
                    <!--/ header contact text -->
            </div>
            <!--/ HEADER ACTION left -->
        </div>
        <!--/ top-header -->

        <!-- separator -->
        <div class="separator"></div>
        <!--/ separator -->

        <!-- left side -->
        <!-- logo container-->
        <div class="logo-container hasInfoCard logosize--yes">
            <!-- Logo -->
            <h1 class="site-logo logo" id="logo">
                <a href="index.php" title="">
                    <img src="images/logo.png" class="logo-img" alt="SP" title="Seguridad Privada" />
                </a>
            </h1>
            <!--/ Logo -->

            <!-- InfoCard -->
            <div id="infocard" class="logo-infocard">
                <div class="custom">
                    <div class="row">
                        <div class="col-sm-5">

                            <p style="text-align: center;">
                                <img src="images/favicons/favicon-16x16.png" class="" alt="SP" title="SP"/>
                            </p>

                        </div>
                        <!--/ col-sm-5 -->

                        <div class="col-sm-7">
                            <div class="custom contact-details">
                                <p>
                                    <strong>T +54 (221) 423 4141 </strong>| <strong>T 0-810-86453  </strong>
                                    <br>
                                    Email:&nbsp;<a href="mailto:info@splaplata.com.ar">info@splaplata.com.ar</a>
                                </p>
                                <p>
                                    Avenida 13 N° 716 <br>
                                    La Plata - Bs As
                                </p>

                            </div>
                            <div style="height:20px;">
                            </div>
                            <!-- Social links clean style -->
                            <ul class="social-icons sc--clean">
                           
                                <li><a href="https://www.facebook.com/saseguridadprivadasa/?fref=ts" target="_self" class="icon-facebook" title="Facebook"></a></li>
                            </ul>
                            <!--/ Social links clean style -->
                        </div>
                        <!--/ col-sm-7 -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ custom -->
            </div>
            <!--/ InfoCard -->

        </div>
        <!--/ logo container-->

        <!-- separator -->
        <div class="separator visible-xxs"></div>
        <!--/ separator -->

        <!-- responsive menu trigger -->
        <div id="zn-res-menuwrapper">
            <a href="#" class="zn-res-trigger zn-header-icon"></a>
        </div>
        <!--/ responsive menu trigger -->

        <!-- main menu -->
        <div id="main-menu" class="main-nav zn_mega_wrapper">
            <ul id="menu-main-menu" class="main-menu zn_mega_menu">
                <li class="menu-item-has-children menu-item-mega-parent"><a href="index.php">INICIO</a>

                </li>
                <li class="menu-item-has-children menu-item-mega-parent"><a href="nosotros.php">NOSOTROS</a>

                </li>
                <li class="menu-item-has-children"><a href="#">PRODUCTOS Y SERVICIOS</a>
                    <ul class="sub-menu clearfix">
                        <li><a href="antientradera.php">ANTIENTRADERAS</a></li>
                        <li><a href="incendio.php">INCENDIO</a></li>
                        <li><a href="monitoreo.php">MONITOREO</a></li>
                        <li><a href="camaras.php">CÁMARAS</a></li>
                        <li><a href="fisica.php">VIGILANCIA FÍSICA</a></li>

                    </ul>
                </li>

                <li class="menu-item-has-children"><a href="rrhh.php">RR.HH</a>
                </li>
                <li class="menu-item-has-children"><a href="clientes.php">ÁREA DE CLIENTES</a>

                </li>
            </ul>
        </div>
        <!--/ main menu -->

        <!-- right side -->
        <!-- Call to action ribbon Free Quote -->
        <a href="contacto.php" id="ctabutton" class="ctabutton kl-cta-ribbon" title="ÁREA DE CLIENTES" target="_self"><strong>COTIZAR</strong>UN SERVICIO </a>
        <!--/ Call to action ribbon Free Quote -->


    </div>
    <!--/ siteheader-container -->
</header>
<!-- / Header style 1 -->