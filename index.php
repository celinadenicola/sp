<!doctype html>
<html class="no-js" lang="es" xmlns="http://www.w3.org/1999/html">
<head>
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

	<!-- Palabras claves y descripción-->

	<meta name="keywords" content="SP La Plata, SP, Seguriad Privada, Alarmas, incendio, antientradera, antipánico, botón antipánico, asistencia, seguridad privada la plata, alarma hogar, monitoreo, camaras de seguridad, cámaras" />
	<meta name="description" content="Seguridad Privada La Plata -SP-">

	<!-- Title -->
	<title>SP Seguridad Privada La Plata</title>

	<?php include('inc/head.php');?>

</head>

<body class="">



	<!-- Page Wrapper -->
	<div id="page_wrapper">
		<?php include('inc/header.php');?>



		<!-- Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->
		<div class="kl-slideshow iosslider-slideshow uh_light_gray maskcontainer--shadow_ud iosslider--custom-height scrollme">
			<!-- Loader -->
			<div class="kl-loader">
				<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z" transform="rotate(98.3774 20 20)"><animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform></path></svg>
			</div>
			<!-- Loader -->

			<div class="bgback">
			</div>

			<!-- Animated Sparkles -->
			<div class="th-sparkles">
			</div>

			<!-- iOS Slider wrapper with animateme scroll efect -->
			<div class="iosSlider kl-slideshow-inner animateme" data-trans="6000" data-autoplay="1" data-infinite="true" data-when="span" data-from="0" data-to="0.75" data-translatey="300" data-easing="linear">
				<!-- Slides -->
				<div class="kl-iosslider hideControls">



					<!-- Slide 3 -->
					<div class="item iosslider__item">
						<!-- Video background container -->
						<div class="kl-video-container">
							<!-- Video wrapper -->
							<div class="kl-video-wrapper">
								<!-- Self Hosted Video Source -->
								<div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
									"position": "absolute", 
									"loop": true, 
									"autoplay": true, 
									"muted": true, 
									"mp4": "videos/sp.mp4",
									"poster": "videos/Working-Space.jpg",
									"video_ratio": "1.7778" }'>
								</div>
								<!--/ Self Hosted Video Source -->
							</div>
							<!--/ Video wrapper -->
						</div>
						<!--/ Video background container -->

						<!--/ Color overlay -->
						<!-- Captions container -->
						<div class="container kl-iosslide-caption kl-ioscaption--style6 fromleft klios-aligncenter kl-caption-posv-middle">
							<!-- Captions animateme wrapper -->
							<div class="animateme">
								<!-- Main Big Title -->
								<h2 class="main_title"><span>ESPECIALISTAS <strong>EN SEGURIDAD</strong> </span></h2>
								<!-- Main Big Title -->



								<div class="klios-playvid">

									<!-- Sistema Antientradera-->
									<a href="antientradera.php"  class="circled-icon ci-large">

										<img src="images/iconos/mdi.png">
										<p>ANTIENTRADERA</p>
									</a>
									<!--Sistema de Prevención de Incendios -->
									<a href="incendio.php" class="circled-icon ci-large">

										<img src="images/iconos/incendio.png">
										<p>INCENDIO</p>

									</a>
									<!--Monitoreo - Alarma Monitoreada-->
									<a href="monitoreo.php" class="circled-icon ci-large">

										<img src="images/iconos/monitoreo.png">
										<p>MONITOREO</p>

									</a>

									<!-- Seguridad Física -->
									<a href="fisica.php" class="circled-icon ci-large">

										<img src="images/iconos/fisica.png">
										<p>SEGURIDAD FÍSICA</p>

									</a>

									<!-- Cámaras de Seguridad  -->
									<a href="camaras.php" class="circled-icon ci-large">

										<img src="images/iconos/camara.png">
										<p>CÁMARAS</p>

									</a>

								</div>

							</div>
							<!--/ Captions animateme wrapper -->
						</div>
						<!--/ Captions container -->

					</div>
					<!--/ Slide 3 -->


				</div>
				<!--/ Slides -->


			</div>
			<!--/ iOS Slider wrapper with animateme scroll efect -->

			<div class="scrollbarContainer">
			</div>

			<!-- Bottom mask style 2 -->
			<div class="kl-bottommask kl-bottommask--shadow_ud">
			</div>
			<!--/ Bottom mask style 2 -->
		</div>
		<!--/ Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->

		<!-- Action Box - Style 3 section with custom top padding and white background color -->
		<section class="hg_section bg-white ptop-0">
			<div class="container">
				<div class="row">
					<!--/ MDI -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
							<div class="action_box_inner">
								<div class="action_box_content">
									<div CLASS="col-md-6">
										<div class="telefono">
											<img src="images/mdi/telefono.png" class="img-responsive"> </div>									</div>
									<div class="col-md-6 pull-right">
										<h4 class="text">
											<span class="fw-thin">MONITOREO </br><span class="fw-semibold">ANTIENTRADERA</span> </span></h4>
								<p>Entrá tranquilo a tu casa. Es fácil, rápido y seguro. Desde tu celular, las 24hs.</p>
										<a class="btn btn-lined ac-btn" href="antientradera.php" target="_blank">CONSULTAR</a>
									</div>

								</div>

								</div>
								<!--/ action_box_content -->
							</div>
							<!--/ action_box_inner -->
						</div>
						<!--/ action_box style3 -->
					<!--/MDI -->
					<!--ATEMPO -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
							<div class="action_box_inner">
								<div class="action_box_content">
									<div CLASS="col-md-6">
										<div class="telefono">
											<img src="images/atempo/pulsera.png" class="img-responsive"> </div>									</div>
									<div class="col-md-6 pull-right">
										<h4 class="text">
											<span class="fw-thin">TELEASISTENCIA</br><span class="fw-semibold">DOMICILIARIA</span> </span></h4>
										<p>La tranquilidad que necesitás, la independencia que ellos quieren.</p>
										<a class="btn btn-lined ac-btn" href="http://www.atemponet.com" target="_blank">CONSULTAR</a>
									</div>

								</div>

							</div>
							<!--/ action_box_content -->
						</div>
						<!--/ action_box_inner -->
					</div>
					<!--/ action_box style3 -->
					<!--/ATEMPO -->
					
					
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Action Box - Style 3 section with custom top padding and white background color -->


		<?php include('inc/footer.php');?>


	</div>
	<!--/ Page Wrapper -->




	

	<!-- ToTop trigger -->
	<a href="#" id="totop">TOP</a>
	<!--/ ToTop trigger -->


	


	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/kl-plugins.js"></script>

	<!-- JS FILES // Loaded on this page -->
	<!-- Requried js script for Slideshow Scroll effect -->
	<script type="text/javascript" src="js/plugins/scrollme/jquery.scrollme.js"></script>

	<!-- Required js script for iOS Slider -->
	<script type="text/javascript" src="js/plugins/_sliders/ios/jquery.iosslider.min.js"></script>

	<!-- Required js trigger for iOS Slider -->
	<script type="text/javascript" src="js/trigger/slider/ios/kl-ios-slider.js"></script>

	<!-- CarouFredSel - Required js script for Screenshot box / Partners Carousel -->
	<script type="text/javascript" src="js/plugins/_sliders/caroufredsel/jquery.carouFredSel-packed.js"></script>

	<!-- Required js trigger for Screenshot Box Carousel -->
	<script type="text/javascript" src="js/trigger/kl-screenshot-box.js"></script>

	<!-- Required js trigger for Partners Carousel -->
	<script type="text/javascript" src="js/trigger/kl-partners-carousel.js"></script>	

	<!-- Custom Kallyas JS codes -->
	<script type="text/javascript" src="js/kl-scripts.js"></script>

	<!-- Custom user JS codes -->
	<script type="text/javascript" src="js/kl-custom.js"></script>

	<!-- Modernizr script -->
	<script type="text/javascript">
		//use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
		Modernizr.load([
			{
				// test for media query support, if not load respond.js
				test : Modernizr.mq('only all'),
				// If not, load the respond.js file
				nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
			}
		]);
	</script>



</body>
</html>