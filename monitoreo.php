<!doctype html>
<html class="no-js" lang="es">
<head>

    <!-- meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
    <!-- Palabras claves y descripción-->
    <meta name="keywords" content="SP La Plata, SP, Seguriad Privada, Alarmas, incendio, antientradera, antipánico, botón antipánico, asistencia, seguridad privada la plata, alarma hogar, monitoreo, camaras de seguridad, cámaras" />
    <meta name="description" content="Seguridad Privada La Plata -SP-">
    <!-- Title -->
    <title>SP Seguridad Privada La Plata - Alarma monitoreada </title>

    <?php include('inc/head.php');?>

</head>

<body class="">



<!-- Page Wrapper -->
<div id="page_wrapper">
    <!-- Header style 1 -->
    <?php include('inc/header.php');?>
    <!-- Slideshow static content element + Bottom mask style 2  -->
    <div class="kl-slideshow static-content__slideshow uh_light_gray maskcontainer--shadow_ud">
        <div class="bgback">
        </div>

        <!-- Static content wrapper with custom minimum height (500px) = .min-500 -->
        <div class="kl-slideshow-inner static-content__wrapper min-350">
            <!-- Static content source -->
            <div class="static-content__source">
                <!-- Background -->
                <div class="kl-bg-source">
                    <!-- Background image -->
                    <div class="kl-bg-source__bgimage" style="background-image:url(images/monitoreo/monitoreo2.jpg); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover;">
                    </div>
                    <!-- Background image -->

                    <!-- Gradient overlay -->
                    <div class="kl-bg-source__overlay" style="background:rgba(30,115,190,0.3); background: -moz-linear-gradient(left, rgba(30,115,190,0.3) 0%, rgba(53,53,53,0.3) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(30,115,190,0.3)), color-stop(100%,rgba(53,53,53,0.3))); background: -webkit-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -o-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -ms-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: linear-gradient(to right, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%);">
                    </div>
                    <!--/ Gradient overlay -->
                </div>
                <!--/ Background -->

                <!-- Animated Sparkles -->
                <div class="th-sparkles"></div>
                <!--/ Animated Sparkles -->
            </div>
            <!--/ .static-content__source -->

            <!-- Static content container -->
            <div class="static-content__inner container">
                <!-- Container with safe padding default top 150px -->
                <div class="kl-slideshow-safepadding sc__container ">
                    <!-- Static content wrapper -->
                    <div class="static-content default-style">
                        <!-- Title -->
                        <h3 class="static-content__subtitle text-left animated fadeInLeft">
								<span class="fw-thin">Tu familia,<span class="fw-semibold"> siempre protegida <span data-rel="tooltip" data-placement="top" title="" data-animation="true" data-original-title="Highly premium actually"></span></span>
								<br>
								</span>
                        </h3>
                        <!--/ Title -->

                        <!-- Info pop-up fade animation left aligned with top arrow -->

                        <!--/ Info pop-up fade animation left aligned with top arrow -->
                    </div>
                    <!--/ .static-content -->
                </div>
                <!--/ Container with safe padding default top 150px -->
            </div>
            <!--/ .kl-slideshow-inner__inner -->
        </div>
        <!--/ Static content wrapper with custom minimum height (500px) = .min-500 -->

        <!-- Bottom mask style 2  -->
        <div class="kl-bottommask kl-bottommask--shadow_ud">
        </div>
        <!--/ Bottom mask style 2  -->
    </div>
    <!--/ Slideshow static content element + Bottom mask style 2  -->

    <!-- Title & sub-title section with custom top padding -->
    <section class="hg_section ptop-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- Title element -->
                    <div class="col-md-5">
                        <img src="images/monitoreo/casa.jpg">

                    </div>
                    <div class="text-left tbk-symbol--line tbk-icon-pos--after-title clearfix col-md-7">
                        <!-- Title with custom font size, black color and semibold style -->
                        <h2 class="black fs-34 fw-semibold"> Alarma monitoreada</h2>

                        <!-- Title bottom symbol -->
                        <div class="tbk__symbol ">
                            <span></span>
                        </div>
                        <!--/ Title bottom symbol -->

                        <!-- Sub-title with custom font size, lightgray color and thin style  -->
                        <h4 class="tbk__subtitle fs-30 light-gray fw-thin">

                            Protegé tu hogar, comercio o empresa garantizando la máxima seguridad, dando aviso inmediato a la policía y enviando móviles de nuestra empresa.
                        </h4>

                        <h4 class="tbk__subtitle fs-30 light-gray fw-thin">
                            Nuestro servicio de alarmas monitoreadas cuenta con la tecnología más eficiente.</h4>

                            <h4 class="tbk__subtitle fs-30 light-gray fw-thin">
                                Nuestros especialistas realizan un proyecto de instalación a medida buscando satisfacer los requisitos de seguridad de cada domicilio.
                                                 </h4>


                    </div>

                    <!--/ Title element -->
                </div>
                <!--/ col-md-12 col-sm-12 -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!--/ Title & sub-title section with custom top padding -->

    <!-- Services boxes modern style section -->
    <section class="hg_section2">
        <div class="container">

            <div class="row">
                <h3 class="black fs-34 fw-semibold" style="color:#cd2122 !important;"> ¿Cómo funciona?</h3>
                <!-- Title bottom symbol -->
             <br><br>

                <div class="col-md-4 col-sm-4">
                    <!-- Services box element modern style -->
                    <div class="services_box services_box--modern sb--hasicon">
                        <!-- Service box wrapper -->
                        <div class="services_box__inner clearfix">
                            <!-- Icon content -->
                            <div class="services_box__icon">
                                <!-- Icon wrapper -->
                                <div class="services_box__icon-inner">
                                    <!-- Icon = .icon-noun_65754  -->
                                    <span class="services_box__fonticon"> 1 </span>
                                </div>
                                <!--/ Icon wrapper -->
                            </div>
                            <!--/ Icon content -->

                            <!-- Content -->
                            <div class="services_box__content">
                                <!-- Title -->
                                <h4 class="services_box__title">Detección</h4>
                                <!--/ Title -->

                                <!-- Description -->
                                <div class="services_box__desc">
                                    <p>
                                       Seguridad Privada ofrece sensores de última tecnología, que aseguran mayor eficacia a la hora de detectar intrusos en la propiedad.
                                    </p>
                                </div>
                                <!--/ Description -->



                                <!-- List wrapper -->
                                <div class="services_box__list-wrapper">
                                    <span class="services_box__list-bg"></span>
                                    <!-- List -->
                                    <ul class="services_box__list">
                                        <li><span class="services_box__list-text"> <img src="images/monitoreo/deteccion.png"></span></li>
                                        <div class="services_box__desc">
                                        <p>
                                            La conexión de la alarma con la Estación de Monitoreo de SP, puede realizarse de distintas formas:
                                        </p>
                                        </div>
                                        <li><span class="services_box__list-text">Enlace telefónico </span></li>
                                        <li><span class="services_box__list-text">Enlace radial </span></li>
                                        <li><span class="services_box__list-text">Enlace GPRS</span></li>


                                    </ul>
                                    <!--/ List -->
                                </div>
                                <!--/ List wrapper -->
                            </div>
                            <!--/ Content -->
                        </div>
                        <!--/ Service box wrapper -->
                    </div>
                    <!--/ Services box element modern style -->
                </div>
                <!--/ col-md-4 col-sm-4 -->

                <div class="col-md-4 col-sm-4">
                    <!-- Services box element modern style -->
                    <div class="services_box services_box--modern sb--hasicon">
                        <!-- Service box wrapper -->
                        <div class="services_box__inner clearfix">
                            <!-- Icon content -->
                            <div class="services_box__icon">
                                <!-- Icon wrapper -->
                                <div class="services_box__icon-inner">
                                    <!-- Icon = .icon-process2  -->
                                    <span class="services_box__fonticon"> 2 </span>
                                </div>
                                <!--/ Icon wrapper -->
                            </div>
                            <!--/ Icon content -->

                            <!-- Content -->
                            <div class="services_box__content">
                                <!-- Title -->
                                <h4 class="services_box__title">Monitoreo</h4>
                                <!--/ Title -->

                                <!-- Description -->
                                <div class="services_box__desc">
                                    <p>
                                        En caso de recibir una señal de alerta desde la propiedad en nuestra Estación de Monitoreo, se activa un operativo de respuesta.

                                    </p>
                                </div>
                                <!--/ Description -->

                                <!-- List wrapper -->
                                <div class="services_box__list-wrapper">
                                    <span class="services_box__list-bg"></span>
                                    <!-- List -->
                                    <ul class="services_box__list">
                                        <li><span class="services_box__list-text"><img src="images/monitoreo/monitoreo2.png"></span></li>
                                        <div class="services_box__desc">
                                            <p>
                                                La central de alarma permite detectar diferentes tipos de eventos como:                                            </p>

                                            </p>
                                        </div>
                                        <li><span class="services_box__list-text">Robo</span></li>
                                        <li><span class="services_box__list-text">Pánico</span></li>
                                        <li><span class="services_box__list-text">Incendio</span></li>
                                        <li><span class="services_box__list-text">Alarma médica</span></li>
                                        <li><span class="services_box__list-text">Desactivación forzada</span></li>
                                    </ul>
                                    <!--/ List -->
                                </div>
                                <!--/ List wrapper -->
                            </div>
                            <!--/ Content -->
                        </div>
                        <!--/ Service box wrapper -->
                    </div>
                    <!--/ Services box element modern style -->
                </div>
                <!--/ col-md-4 col-sm-4 -->

                <div class="col-md-4 col-sm-4">
                    <!-- Services box element modern style -->
                    <div class="services_box services_box--modern sb--hasicon">
                        <!-- Service box wrapper -->
                        <div class="services_box__inner clearfix">
                            <!-- Icon content -->
                            <div class="services_box__icon">
                                <!-- Icon wrapper -->
                                <div class="services_box__icon-inner">
                                    <!-- Icon = .icon-process3  -->
                                    <span class="services_box__fonticon"> 3 </span>
                                </div>
                                <!--/ Icon wrapper -->
                            </div>
                            <!--/ Icon content -->

                            <!-- Content -->
                            <div class="services_box__content">
                                <!-- Title -->
                                <h4 class="services_box__title">Respuesta</h4>
                                <!--/ Title -->

                                <!-- Description -->
                                <div class="services_box__desc">
                                    <p>
                                        Personal de nuestra flota de seguridad, se presenta en el lugar del hecho para prestar apoyo al cliente y cooperar con las Fuerzas de Seguridad y Asistencia.                                    </p>
                                </div>
                                <!--/ Description -->

                                <!-- List wrapper -->
                                <div class="services_box__list-wrapper">
                                    <span class="services_box__list-bg"></span>
                                    <!-- List -->
                                    <ul class="services_box__list">
                                        <li><span class="services_box__list-text"><img src="images/monitoreo/asistencia.png"></span></li>
                                        <div class="services_box__desc">
                                            <p>
                                                Según el tipo de siniestro nos ponemos en contacto con el organismo correspondiente:                                         </p>

                                            </p>
                                        </div>
                                        <li><span class="services_box__list-text">Policía</span></li>
                                        <li><span class="services_box__list-text">Bomberos</span></li>
                                        <li><span class="services_box__list-text">Emergencias médicas</span></li>
                                    </ul>
                                    <!--/ List -->
                                </div>
                                <!--/ List wrapper -->
                            </div>
                            <!--/ Content -->
                        </div>
                        <!--/ Service box wrapper -->
                    </div>
                    <!--/ Services box element modern style -->
                </div>
                <!--/ col-md-4 col-sm-4 -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!--/ Services boxes modern style section -->

    <!-- Title + description section with custom bottom padding -->




    <!-- Call out banner style 3 section with white background color and custom top padding -->
    <section class="hg_section ptop-60 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <!-- Call-out banner element - style 3 -->
                    <div class="callout-banner clearfix">
                        <div class="row">
                            <div class="col-sm-12">

                                <h2> Qué beneficios tiene el servicio de monitoreo de alarma?</h2>
                                <!-- Title -->
<br>
                                <div class="col-md-4"><h3 class="m_title"><img src="images/monitoreo/reloj.png"><span class="fw-thin">  Protección las <span class="fw-semibold">24 hs</span></span></h3></div>
                                <!--/ Title -->
                                <div class="col-md-4"><h3 class="m_title"><span class="fw-thin"> <img src="images/monitoreo/monitoreo.png">  Respuesta <span class="fw-semibold"> inmediata </span></span></h3></div>
                                <div class="col-md-4"><h3 class="m_title"><span class="fw-thin"> <img src="images/monitoreo/movil.png">  Envío de  <span class="fw-semibold">móvil al lugar</span></span></h3></div>

                            </div>
                            <!--/ col-sm-10 -->


                            <!--/ col-sm-2 -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ Call-out banner element - style 3 -->
                </div>
                <!--/ col-md-12 col-sm-12 -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!--/ Call out banner style 3 section with white background color and custom top padding -->


    <!-- Footer - Default Style -->
    <?php include('inc/footer.php');?>

</div>
<!--/ Page Wrapper -->


<!-- Login Panel content -->
<div id="login_panel" class="mfp-hide loginbox-popup auth-popup">
    <div class="inner-container login-panel auth-popup-panel">
        <h3 class="m_title m_title_ext text-custom auth-popup-title tcolor">SIGN IN YOUR ACCOUNT TO HAVE ACCESS TO DIFFERENT FEATURES</h3>
        <form class="login_panel" name="login_form" method="post" action="#">
            <div class="form-group kl-fancy-form">
                <input type="text" id="kl-username" name="log" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="eg: james_smith">
                <label class="kl-font-alt kl-fancy-form-label">USERNAME</label>
            </div>
            <div class="form-group kl-fancy-form">
                <input type="password" id="kl-password" name="pwd" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="type password">
                <label class="kl-font-alt kl-fancy-form-label">PASSWORD</label>
            </div>
            <label class="auth-popup-remember" for="kl-rememberme"><input type="checkbox" name="rememberme" id="kl-rememberme" value="forever" class="auth-popup-remember-chb"> Remember Me </label>
            <input type="submit" id="login" name="submit_button" class="btn zn_sub_button btn-fullcolor btn-md" value="LOG IN">
            <input type="hidden" value="login" class="" name="form_action"><input type="hidden" value="login" class="" name="action">
            <input type="hidden" value="#" class="" name="submit">
            <div class="links auth-popup-links">
                <a href="#register_panel" class="create_account auth-popup-createacc kl-login-box auth-popup-link">CREATE AN ACCOUNT</a><span class="sep auth-popup-sep"></span><a href="#forgot_panel" class="kl-login-box auth-popup-link">FORGOT YOUR PASSWORD?</a>
            </div>
        </form>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>
<div id="register_panel" class="mfp-hide loginbox-popup auth-popup">
    <div class="inner-container register-panel auth-popup-panel">
        <h3 class="m_title m_title_ext text-custom auth-popup-title">CREATE ACCOUNT</h3>
        <form class="register_panel" name="login_form" method="post" action="#">
            <div class="form-group kl-fancy-form ">
                <input type="text" id="reg-username" name="user_login" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="type desired username"><label class="kl-font-alt kl-fancy-form-label">USERNAME</label>
            </div>
            <div class="form-group kl-fancy-form">
                <input type="text" id="reg-email" name="user_email" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="your-email@website.com"><label class="kl-font-alt kl-fancy-form-label">EMAIL</label>
            </div>
            <div class="form-group kl-fancy-form">
                <input type="password" id="reg-pass" name="user_password" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****"><label class="kl-font-alt kl-fancy-form-label">PASSWORD</label>
            </div>
            <div class="form-group kl-fancy-form">
                <input type="password" id="reg-pass2" name="user_password2" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****"><label class="kl-font-alt kl-fancy-form-label">CONFIRM PASSWORD</label>
            </div>
            <div class="form-group">
                <input type="submit" id="signup" name="submit" class="btn zn_sub_button btn-block btn-fullcolor btn-md" value="CREATE MY ACCOUNT">
            </div>
            <div class="links auth-popup-links">
                <a href="#login_panel" class="kl-login-box auth-popup-link">ALREADY HAVE AN ACCOUNT?</a>
            </div>
        </form>
    </div>
</div>
<div id="forgot_panel" class="mfp-hide loginbox-popup auth-popup forgot-popup">
    <div class="inner-container forgot-panel auth-popup-panel">
        <h3 class="m_title m_title_ext text-custom auth-popup-title">FORGOT YOUR DETAILS?</h3>
        <form class="forgot_form" name="login_form" method="post" action="#">
            <div class="form-group kl-fancy-form">
                <input type="text" id="forgot-email" name="user_login" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="...">
                <label class="kl-font-alt kl-fancy-form-label">USERNAME OR EMAIL</label>
            </div>
            <div class="form-group">
                <input type="submit" id="recover" name="submit" class="btn btn-block zn_sub_button btn-fullcolor btn-md" value="SEND MY DETAILS!">
            </div>
            <div class="links auth-popup-links">
                <a href="#login_panel" class="kl-login-box auth-popup-link">AAH, WAIT, I REMEMBER NOW!</a>
            </div>
        </form>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>
<!--/ Login Panel content -->


<!-- ToTop trigger -->
<a href="#" id="totop">TOP</a>
<!--/ ToTop trigger -->





<!-- JS FILES // These should be loaded in every page -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/kl-plugins.js"></script>

<!-- Custom Kallyas JS codes -->
<script type="text/javascript" src="js/kl-scripts.js"></script>

<!-- Custom user JS codes -->
<script type="text/javascript" src="js/kl-custom.js"></script>

<!-- Modernizr script -->
<script type="text/javascript">
    //use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
    Modernizr.load([
        {
            // test for media query support, if not load respond.js
            test : Modernizr.mq('only all'),
            // If not, load the respond.js file
            nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
        }
    ]);
</script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXX-X', 'auto');
  ga('send', 'pageview');
</script>
-->

</body>
</html>