<!doctype html>
<html class="no-js" lang="es">
<head>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

	<!-- Palabras claves y descripción-->

	<meta name="keywords" content="SP La Plata, SP, Seguriad Privada, Alarmas, incendio, antientradera, antipánico, botón antipánico, asistencia, seguridad privada la plata, alarma hogar, monitoreo, camaras de seguridad, cámaras" />
	<meta name="description" content="Seguridad Privada La Plata -SP-">
	<!-- Title -->
	<title>Contacto Seguridad Privada SP</title>
	<?php include('inc/head.php');?>

</head>

<body class="">

<?php include('inc/header.php');?>




<!-- Slideshow static content element + Bottom mask style 2  -->
<div class="kl-slideshow static-content__slideshow uh_light_gray maskcontainer--shadow_ud">
	<div class="bgback">
	</div>

	<!-- Static content wrapper with custom minimum height (500px) = .min-500 -->
	<div class="kl-slideshow-inner static-content__wrapper min-350">
		<!-- Static content source -->
		<div class="static-content__source">
			<!-- Background -->
			<div class="kl-bg-source">
				<!-- Background image -->
				<div class="kl-bg-source__bgimage" style="background-image:url(images/contacto/contacto2.png); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover;">
				</div>
				<!-- Background image -->

				<!-- Gradient overlay -->
				<div class="kl-bg-source__overlay" style="background:rgba(30,115,190,0.3); background: -moz-linear-gradient(left, rgba(30,115,190,0.3) 0%, rgba(53,53,53,0.3) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(30,115,190,0.3)), color-stop(100%,rgba(53,53,53,0.3))); background: -webkit-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -o-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -ms-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: linear-gradient(to right, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%);">
				</div>
				<!--/ Gradient overlay -->
			</div>
			<!--/ Background -->

			<!-- Animated Sparkles -->
			<div class="th-sparkles"></div>
			<!--/ Animated Sparkles -->
		</div>
		<!--/ .static-content__source -->

		<!-- Static content container -->
		<div class="static-content__inner container">
			<!-- Container with safe padding default top 150px -->
			<div class="kl-slideshow-safepadding sc__container ">
				<!-- Static content wrapper -->
				<div class="static-content default-style">
					<!-- Title -->
					<h3 class="static-content__subtitle text-left animated fadeInLeft">
								<span class="fw-thin">Especialistas<span class="fw-semibold"> en seguridad<span data-rel="tooltip" data-placement="top" title="" data-animation="true" data-original-title="Highly premium actually"></span></span>
								<br>
								</span>
					</h3>
					<!--/ Title -->

					<!-- Info pop-up fade animation left aligned with top arrow -->

					<!--/ Info pop-up fade animation left aligned with top arrow -->
				</div>
				<!--/ .static-content -->
			</div>
			<!--/ Container with safe padding default top 150px -->
		</div>
		<!--/ .kl-slideshow-inner__inner -->
	</div>
	<!--/ Static content wrapper with custom minimum height (500px) = .min-500 -->


	<!-- Content section -->
	<section class="hg_section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<!-- Title element with bottom line and sub-title with custom paddings -->
					<div class="kl-title-block clearfix tbk--left tbk-symbol--line tbk-icon-pos--after-title ptop-35 pbottom-65">
						<!-- Title with custom montserrat font, size and black color -->
						<h2 class="tbk__title montserrat fs-34 black"><strong>Quienes Somos</strong></h2>

						<!-- Title bottom symbol -->
						<div class="tbk__symbol ">
							<span></span>
						</div>
						<!--/ Title bottom symbol -->

						<!-- Sub-title with custom font size and thin style -->
						<h4 class="tbk__subtitle fs-30 fw-thin">
							Nos dedicamos a la <strong>seguridad electrónica y monitoreo de alarmas desde 1992</strong> ubicada en la ciudad de La Plata.
						</h4>
					</div>
					<!--/ Title element with bottom line and sub-title -->
				</div>
				<!--/ col-md-12 col-sm-12 -->

				<div class="col-md-4 col-sm-4">
					<!-- Title element with bottom line -->
					<div class="kl-title-block clearfix tbk-symbol--line tbk-icon-pos--after-title">
						<!-- Title with custom font size, gray color and extrabold style -->
						<h3 class="tbk__title fs-36 gray fw-extrabold">
							Somos uno de los líderes de la región en el mercado.</h3>

						<!-- Title bottom symbol -->
						<div class="tbk__symbol ">
							<span></span>
						</div>
						<!--/ Title bottom symbol -->
					</div>
					<!--/ Title element with bottom line -->
				</div>
				<!--/ col-md-4 col-sm-4 -->

				<div class="col-md-4 col-sm-4">
					<!-- Text box element -->
					<div class="text_box">
						<p>
							En S.P. Seguridad Privada S.A. brindamos un servicio personalizado, interesándonos en cada cliente y desarrollando la solución que mejor se adapte a cada uno.
						</p>

					</div>
					<!--/ Text box element -->
				</div>
				<!--/ col-md-4 col-sm-4 -->

				<div class="col-md-4 col-sm-4">
					<!-- Text box element -->
					<div class="text_box">
						<p>
							Proveemos e instalamos los equipos más confiables en el mercado de seguridad con nuestra flota de instaladores altamente capacitados, buscando alcanzar la satisfacción de nuestros clientes, garantizándoles la tranquilidad de estar bien protegidos.
						</p>
					</div>
					<!--/ Text box element -->
				</div>
				<!--/ col-md-4 col-sm-4 -->
			</div>
			<!--/ row -->
		</div>
		<!--/ container -->
	</section>
	<!--/ Content section -->


</div>
<!--/ Slideshow static content element + Bottom mask style 2  -->


				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Contact form element section -->
<?php include('inc/footer.php');?>
	</div>
	<!--/ Page Wrapper -->


	

	<!-- ToTop trigger -->
	<a href="#" id="totop">TOP</a>
	<!--/ ToTop trigger -->


	


	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/kl-plugins.js"></script>

	<!-- JS FILES // Loaded on this page -->
	
	<!-- Custom Kallyas JS codes -->
	<script type="text/javascript" src="js/kl-scripts.js"></script>

	<!-- Custom user JS codes -->
	<script type="text/javascript" src="js/kl-custom.js"></script>

	<!-- Modernizr script -->
	<script type="text/javascript">
		//use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
		Modernizr.load([
			{
				// test for media query support, if not load respond.js
				test : Modernizr.mq('only all'),
				// If not, load the respond.js file
				nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
			}
		]);
	</script>


</body>
</html>