<?php

$method = strtoupper($_SERVER['REQUEST_METHOD']);
if('POST' != $method) {
	exit('Invalid request');
}

// CONFIGURE RECAPTCHA
define('GR_SECRET', '6Lfqew4UAAAAAEm_a86gZlvO8pKTiG_roPz5ok0F');
define('GR_URL', 'https://www.google.com/recaptcha/api/siteverify');


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "john.doe@yourdomain.com";
if ($_POST["opcion"] == 1)
{
    $address="brovejuli@gmail.com";
}
if ($_POST["opcion"] == 2)
{
    $address="julieta@estudioumo.com.ar";

}
if ($_POST["opcion"] == 3)

{
    $address="splaplata2@gmail.com ";

}



function validateRecaptcha( $secret, $response, $url = GR_URL ){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	$params = array(
		'secret' => urlencode($secret),
		'response' => urlencode($response),
	);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$result = json_decode($result);
	curl_close($ch);
	return (isset($result->success) && $result->success);
}


if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

// Sanitize request
$name = (isset($_POST['name']) ? strip_tags($_POST['name']) : '');
$lastname = (isset($_POST['lastname']) ? strip_tags($_POST['lastname']) : '');
$email = (isset($_POST['email']) ? strip_tags($_POST['email']) : '');
$direccion = (isset($_POST['direccion']) ? strip_tags($_POST['direccion']) : '');
$telefono = (isset($_POST['telefono']) ? strip_tags($_POST['telefono']) : '');
$cliente = (isset($_POST['cliente']) ? strip_tags($_POST['cliente']) : '');
$opcion = (isset($_POST['opcion']) ? strip_tags($_POST['opcion']) : '');

$subject = (isset($_POST['subject']) ? strip_tags($_POST['subject']) : '');

$carlist = (isset($_POST['carlist']) ? strip_tags($_POST['carlist']) : '');
$message= (isset($_POST['message']) ? strip_tags($_POST['message']) : '');
$g_response = (isset($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response'] : '');

// Validate inputs
if(empty($name)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Por favor, escriba su nombre.</p></div>';
	exit();
}
if(empty($lastname)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Por favor, escriba su apellido.</p></div>';
	exit();
}
if(empty($email)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Necesitamos su email.</p></div>';
	exit();
}
if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Por favor, escriba un email válido.</p></div>';
	exit();
}
if(empty($subject)){
	$subject = 'SP, fuiste contactado desde la web por ' . $name;
}
if(empty($message)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Por favor, escriba un mensaje.</p></div>';
	exit();
}

if(get_magic_quotes_gpc()) {
	$message = stripslashes($message);
}
if(empty($g_response)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Por favor, confirme no ser un robot.</p></div>';
	exit();
}
if(!validateRecaptcha(GR_SECRET, $g_response, GR_URL)){
	echo '<div class="alert alert-warning error"><p><strong>Atención!</strong> Captcha incorrecto.</p></div>';
	exit();
}


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name $lastname" . PHP_EOL . PHP_EOL;

$e_content = "
MENSAJE: $message
NUMERO DE CLIENTE: $cliente
DIRECCIÓN: $direccion
TELÉFONO: $telefono" . PHP_EOL . PHP_EOL;

$e_reply = "Podés contactar a  $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );

$headers = "From: $email" . PHP_EOL;
$headers .= "Reply-To: $email" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

if(mail($address, $subject, $msg, $headers)) {

	// Email has sent successfully, echo an error message.
	echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">CERRAR</span></button><p>GRACIAS <strong>'.$name.'</strong>, TU MENSAJE FUE ENVIADO.</p></div>';

}
else {
	// Email has NOT been sent successfully, echo an error message.
	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">CERRAR</span></button><div class="alert alert-danger"><strong>ERROR!</strong>TU MENSAJE NO FUE ENVIADO, INTENTALO DE NUEVO..</div>';
}


