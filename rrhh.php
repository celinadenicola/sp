<!doctype html>
<html class="no-js" lang="es">
<head>

    <!-- meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

    <!-- Palabras claves y descripción-->

    <meta name="keywords" content="SP La Plata, SP, Seguriad Privada, Alarmas, incendio, antientradera, antipánico, botón antipánico, asistencia, seguridad privada la plata, alarma hogar, monitoreo, camaras de seguridad, cámaras" />
    <meta name="description" content="Seguridad Privada La Plata -SP-">
    <!-- Title -->
    <title>Recursos Humanos Seguridad Privada SP</title>

    <?php include('inc/head.php');?>
</head>

<body class="">

<?php include('inc/header.php');?>




<!-- Slideshow static content element + Bottom mask style 2  -->
<div class="kl-slideshow static-content__slideshow uh_light_gray maskcontainer--shadow_ud">
    <div class="bgback">
    </div>

    <!-- Static content wrapper with custom minimum height (500px) = .min-500 -->
    <div class="kl-slideshow-inner static-content__wrapper min-350">
        <!-- Static content source -->
        <div class="static-content__source">
            <!-- Background -->
            <div class="kl-bg-source">
                <!-- Background image -->
                <div class="kl-bg-source__bgimage" style="background-image:url(images/contacto/contacto.png); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover;">
                </div>
                <!-- Background image -->

                <!-- Gradient overlay -->
                <div class="kl-bg-source__overlay" style="background:rgba(30,115,190,0.3); background: -moz-linear-gradient(left, rgba(30,115,190,0.3) 0%, rgba(53,53,53,0.3) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(30,115,190,0.3)), color-stop(100%,rgba(53,53,53,0.3))); background: -webkit-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -o-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: -ms-linear-gradient(left, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%); background: linear-gradient(to right, rgba(30,115,190,0.3) 0%,rgba(53,53,53,0.3) 100%);">
                </div>
                <!--/ Gradient overlay -->
            </div>
            <!--/ Background -->

            <!-- Animated Sparkles -->
            <div class="th-sparkles"></div>
            <!--/ Animated Sparkles -->
        </div>
        <!--/ .static-content__source -->

        <!-- Static content container -->
        <div class="static-content__inner container">
            <!-- Container with safe padding default top 150px -->
            <div class="kl-slideshow-safepadding sc__container ">
                <!-- Static content wrapper -->
                <div class="static-content default-style">
                    <!-- Title -->
                    <h3 class="static-content__subtitle text-left animated fadeInLeft">
								<span class="fw-thin">Te asesoramos<span class="fw-semibold"> en seguridad<span data-rel="tooltip" data-placement="top" title="" data-animation="true" data-original-title="Highly premium actually"></span></span>
								<br>
								</span>
                    </h3>
                    <!--/ Title -->


                </div>
                <!--/ .static-content -->
            </div>
            <!--/ Container with safe padding default top 150px -->
        </div>
        <!--/ .kl-slideshow-inner__inner -->
    </div>
    <!--/ Static content wrapper with custom minimum height (500px) = .min-500 -->

    <!-- Bottom mask style 2  -->
    <div class="kl-bottommask kl-bottommask--shadow_ud">
    </div>
    <!--/ Bottom mask style 2  -->
</div>
<!--/ Slideshow static content element + Bottom mask style 2  -->

<!-- Title & sub-title section with custom top padding -->
<section class="hg_section ptop-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <!-- Title -->
                <div class="kl-title-block clearfix text-left tbk-symbol--line tbk-icon-pos--after-title">
                    <h3 class="tbk__title ">TRABAJÁ EN SP</h3>
							<span class="tbk__symbol">
								<span></span>
							</span>
                </div>
                <!--/ Title -->

                <!-- Contact form element -->

                    <p>Si desea formar parte de nuestro staff, por favor complete el siguiente formulario. </p>
                <form name='formulario' id='formulario' method='post' action='contact_process2.php' target='_self' enctype="multipart/form-data">
                    <p class="col-sm-12 kl-fancy-form">
                        <input type="text" name="nombre" id="Nombre" class="form-control" placeholder="Por favor ingrese su nombre" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">NOMBRE Y APELLIDO</label></p>
                    <p class="col-sm-12 kl-fancy-form">
                        <input type="text" name="email" id="email" class="form-control h5-email" placeholder="Por favor ingrese su email" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">EMAIL</label>
                    </p>
                    <p class="col-sm-12 kl-fancy-form">
                        <input type="text" name="asunto" id="asunto" class="form-control" placeholder="Escriba un asunto" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">ASUNTO</label>
                    </p>
                    <p class="col-sm-12 kl-fancy-form">
                        <textarea name="mensaje" id="mensaje" class="form-control" cols="30" rows="10" placeholder="Su mensaje" tabindex="4" required></textarea>
                        <label class="control-label">MENSAJE</label>
                    </p>
                    <p>Adjuntar CV: <input type='file' name='archivo1' id='archivo1'></p>
                    <p>
                        <button class="btn btn-fullcolor" type="submit">ENVIAR</button>                    </p>
                </form>

                <!--/ Contact form element -->
            </div>
            <!--/ col-sm-9 -->

            <div class="col-md-3 col-sm-3">
                <!-- Contact details -->
                <div class="text_box">
                    <img src="images/logo.png"
                    <h3>Avenida 13 N° 716 La Plata - Bs As

                    </h3>
                    <p>
                        TEL: +54 (221) 423 4141<br>
                        FAX: +54 (221) 423 1098
                    </p>
                    <p>
                        <a href="mailto:#">contacto@splaplata.com.ar</a><br>

                    </p>
                </div>
                <!--/ Contact details -->
            </div>
            <!--/ col-md-3 col-sm-3 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!--/ Contact form element section -->
<?php include('inc/footer.php');?>
</div>
<!--/ Page Wrapper -->




<!-- ToTop trigger -->
<a href="#" id="totop">TOP</a>
<!--/ ToTop trigger -->





<!-- JS FILES // These should be loaded in every page -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/kl-plugins.js"></script>

<!-- JS FILES // Loaded on this page -->

<!-- Custom Kallyas JS codes -->
<script type="text/javascript" src="js/kl-scripts.js"></script>

<!-- Custom user JS codes -->
<script type="text/javascript" src="js/kl-custom.js"></script>

<!-- Modernizr script -->
<script type="text/javascript">
    //use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
    Modernizr.load([
        {
            // test for media query support, if not load respond.js
            test : Modernizr.mq('only all'),
            // If not, load the respond.js file
            nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
        }
    ]);
</script>


</body>
</html>